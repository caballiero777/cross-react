import React, { Component } from 'react';
import './List.css';

class List extends Component {
    mapItems = items => items.map(item =>
        (<div className="item" key={item.id}>
            <span className="name">{item.name}</span>
            <span>
                <span className="edit fas fa-pen" onClick={() => this.onEdit(item)}></span>
                <span className="delete fas fa-trash-alt" onClick={() => this.props.onDelete(item.id)}></span>
            </span>
        </div>))

    onAdd = () => {
        const item = { name: this.input.value }
        if (this.state && this.state.editItem) {
            item.id = this.state.editItem.id;
            this.setState({ editItem: undefined });
        }
        this.props.onAdd(item);
        this.input.value = '';
    }

    onEdit = item => {
        this.setState({ editItem: item });
        this.input.value = item.name;
    }

    render() {
        return (
            <div className="list">
                <div className="new-item item">
                    <input type="text" className="new-item__input" ref={input => this.input = input} />
                    <button onClick={this.onAdd.bind(this)}>Add</button>
                </div>
                {this.mapItems(this.props.items)}
            </div>
        );
    }
}

export default List;
