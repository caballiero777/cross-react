export const items = [{
    value: 'Audi',
    id: 1,
    inner: [{
        value: 'TT',
        id: 11,
    },{
        value: 'A1',
        id: 12,
    },{
        value: 'A2',
        id: 13,
    },{
        value: 'A3',
        id: 14
    },{
        value: 'A4',
        id: 15
    },{
        value: 'A5',
        id: 16
    },{
        value: 'A6',
        id: 17
    },{
        value: 'Q7',
        id: 18
    },{
        value: 'Q8',
        id: 19
    },{
        value: 'SQ7',
        id: 20
    }]
},{
    value: 'Honda',
    id: 2,
    inner: [{
        value: 'Civic',
        id: 21
    },{
        value: 'Cesta',
        id: 22
    },{
        value: 'CR-V',
        id: 23
    },{
        value: 'Insight',
        id: 24
    },{
        value: 'Pioneer',
        id: 25
    },{
        value: 'Accord',
        id: 26
    }]
},{
    value: 'Hyndai',
    id: 3,
    inner: [{
        value: 'Accent',
        id: 31
    },{
        value: 'Coupe',
        id: 32
    },{
        value: 'Elantra',
        id: 33
    },{
        value: 'Sonata',
        id: 34
    },{
        value: 'Tucson',
        id: 35
    }]
},{
    value: 'Mitsubishi',
    id: 4,
    inner: [{
        value: 'Galant',
        id: 4
    },{
        value: 'Grandis',
        id: 42
    },{
        value: 'Lancer',
        id: 43
    },{
        value: 'Outlander',
        id: 44
    },{
        value: 'Pajero',
        id: 45
    }]
}];