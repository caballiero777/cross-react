import React, { Component } from 'react';
import './Dropdown.css';

class Dropdown extends Component {
    onChange = event => this.props.onSelect && this.props.onSelect(+event.target.value);

    getOptions = options => options.map(option => (<option className="dropdown__item" key={option.id} value={option.id}>{option.name}</option>))

    render() {
        return (
        <select className="dropdown" onChange={this.onChange}>
            {this.getOptions(this.props.items)}
        </select>
        );
    }
}

export default Dropdown;
