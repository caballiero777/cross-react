import React, { Component } from 'react';
import Dropdown from './Dropdown/Dropdown';
import List from './List/List';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            marks: [],
            models: []
        };

        this.refresh();
    }

    refresh = () => fetch('http://localhost:4000/mark', { method: 'GET' })
        .then(marks => marks.json())
        .then(marks => {
            this.setState({
                marks,
                selectedItemId: this.state.selectedItemId | marks[0].id
            })
            return fetch(`http://localhost:4000/model/${this.state.selectedItemId}`, { method: 'GET' });
        })
        .then(models => models.json())
        .then(models => this.setState({ models }))

    onItemSelected = id => fetch(`http://localhost:4000/model/${id}`, { method: 'GET', })
        .then(models => models.json())
        .then(models => this.setState({ models, selectedItemId: id }));

    getInnerItems = id => this.state.items.find(item => item.id === id).inner;

    addMark = mark =>
        fetch('http://localhost:4000/mark', { method: 'POST', headers: new Headers([['Content-Type', 'application/json']]), body: JSON.stringify(mark) })
        .then(() => this.refresh())

    addModel = model =>
        fetch('http://localhost:4000/model', { method: 'POST', headers: new Headers([['Content-Type', 'application/json']]), body: JSON.stringify({ id: model.id , name: model.name, markId: this.state.selectedItemId }) })
        .then(() => this.refresh())

    deleteMark = id =>
        fetch('http://localhost:4000/mark', { method: 'DELETE', headers: new Headers([['Content-Type', 'application/json']]), body: JSON.stringify({ id }) })
        .then(() => this.refresh())

    deleteModel = id =>
        fetch('http://localhost:4000/model', { method: 'DELETE', headers: new Headers([['Content-Type', 'application/json']]), body: JSON.stringify({ id }) })
        .then(() => this.refresh())

    render() {
        return (
            <div className="App">
                <div className="dropdowns">
                    <Dropdown items={this.state.marks} onSelect={this.onItemSelected} />
                    <Dropdown items={this.state.models} />
                </div>
                <div className="items-list">
                    <List items={this.state.marks} onAdd={this.addMark} onDelete={this.deleteMark} />
                    <List items={this.state.models} onAdd={this.addModel} onDelete={this.deleteModel} />
                </div>
            </div>
        );
    }
}

export default App;
