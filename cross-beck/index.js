var express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express();

app.use(bodyParser.json())
app.use(cors())

app.use(function(req, res, next) {
    console.log(req.method);
    if ('OPTIONS' === req.method) {
      res.send(200);
    }
    else {
      next();
    }
});

const Sequelize = require('sequelize');
const sequelize = new Sequelize('cross', null, null, {
    dialect: 'sqlite',
    storage: './cross.sqlite'
})

sequelize
    .authenticate()
    .then(function (err) {
        console.log('Connection has been established successfully.')
    }, function (err) {
        console.log('Unable to connect to the database:', err)
    })

const Mark = sequelize.define('mark', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: Sequelize.STRING
}, {});
const Model = sequelize.define('model', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: Sequelize.STRING
})

Model.belongsTo(Mark)

sequelize
    .sync()
    .then(function (err) {
        console.log('It worked!')
    }, function (err) {
        console.log('An error occurred while creating the table:', err)
    })

app.delete('/model', function (req, res) {
    Model.destroy({
        where: {
            id: req.body.id
        }
    }).then(number => res.send({ number }))
})

app.post('/model', function (req, res) {
    Model.upsert(req.body).then(result => res.send(result))
})

app.get('/model/:markId', function (req, res) {
    Model.findAll({
        where: {
            markId: req.params.markId
        }
    }).then(models => res.send(models))
})

app.delete('/mark', function (req, res) {
    Mark.destroy({
        where: {
            id: req.body.id
        }
    }).then(number => res.send({ number }))
})

app.post('/mark', function (req, res) {
    console.log(req.body);
    Mark.upsert(req.body).then(result => res.send(result))
})

app.get('/mark', function (req, res) {
    Mark.findAll().then(marks => res.send(marks))
})

app.get('/', function (req, res) {
    res.send('Hello World!')
})

app.listen(4000, function () {
    console.log('Example app listening on port 4000!')
})
